# Changelog

  

All notable changes to this project will be documented in this file.

  

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),

and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

  

## (0.0.4)[https://gitlab.com/etiennetardif24/skyhive/-/tags/v0.0.4] - 2024-04-30

### Added

- MQTT Broker tester app in Python

- The CSharp API has been replace by a C# MQTT Broker, Websocket was meant to be used has its a simple pure web way of doing bidirectionnal messaging over the internet. Because the Arduino is closer to an IOT device then a standard Server of Computer, the MQTT protocol is a more standard way of working with it.

  
  

### Changed

- Added logic to the Arduino code to:

- Validate the hardware connections

- Validate the connection to internet

- Validate the connection to the MQTT Broker

  

### Removed

- C# API has been removed to utilise a C# MQTT Broker

  

## (0.0.3)[https://gitlab.com/etiennetardif24/skyhive/-/tags/v0.0.3] - 2024-04-28

### Added

- Added a changelog

  

### Changed

- The Readme has been updated to reflect the changes to the project.

  

## (v0.0.2)[https://gitlab.com/etiennetardif24/skyhive/-/tags/V0.0.2] - 2024-03-01

### Changed

- The arduino project has been updated to test the hardware ( Functions to test the servo motors and ESC for example)

- Skeleton for Python App has been update to a simple multipage native python application

- Document has been update to reflect the state of the project

  

## (v0.0.1)[https://gitlab.com/etiennetardif24/skyhive/-/tags/v0.0.1] - 2024-02-09

### Added

- Readme file

- Analysis documents

- Basic Arduino project has been started

- Skeleton for API Project

- Skeleton for Python App