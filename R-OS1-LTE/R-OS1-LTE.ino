/*
  Project: Open Source LTE Controlled Drone
  Author: Etienne Tardif
  Last Updated: April 29, 2024
  Version: 0.0.5

  Description: This code acts as the "Operating System" of a basic open source planer drone controlled over LTE from a MQTT Broker
*/
#include <Servo.h>
#include <MKRNB.h>
#include <MQTT.h>

char clientId[] = "R-OS001-LTE";

const char pin[] = "0000"; // SIM Card provider security pin
const char apn[] = "chatrweb.apn"; // SIM Card provider APN server

const char mqttBroker[] = "broker.hivemq.com";
const int mqttPort = 1883; // default MQTT Port
const char topic[] = "SkyHiveArduino0SLTE0.01";

const char login[] = "";
const char password[] = "";

unsigned long lastMillis = 0;

const int motorEscPin = 2; // Pin connected to the PWN wire of the ESC module
const int tailPin = 3;    // Pin connected to the Tail Flap Servo Motor
const int rightPin = 4;   // Pin connected to the Right Wing Servo Motor
const int leftPin = 5;    // Pin connected to the Left Wing Servo Motor

int thrustValue = 131; // Initial motor speed
int leftTilt = 0;      // Initial left wing tilt
int rightTilt = 0;     // Initial right wing tilt
int tailTilt = 0;      // Initial tail tilt

Servo tailWingServo;
Servo rightWingServo;
Servo leftWingServo;

NBClient net;
GPRS gprs;
NB nbAccess;
MQTTClient client;

// Setup function: Initializes hardware and establishes connections
void setup() {
  Serial.begin(115200);

  // Attach servo motors to pins
  tailWingServo.attach(tailPin);
  rightWingServo.attach(rightPin);
  leftWingServo.attach(leftPin);

  // Perform homing to test servo and motor functionality
  homingRoutine();

  // Initialize MQTT client and connect to broker
  client.begin(mqttBroker, mqttPort, net);
  client.onMessage(messageReceived);
  connectToBroker();
}

// Loop function: Continuously checks for MQTT messages and publishes a test message
void loop() {
  client.loop();

  if (!client.connected()) {
    connectToBroker();
  }

  // publish a message roughly every second.
  if (millis() - lastMillis > 1000) {
    lastMillis = millis();
    client.publish(topic, "test");
  }  
}

// Homing routine: Tests servo and motor functionality
void homingRoutine() {
  Serial.println("Starting Homing");
  adjustMotors(115, 115, 115);
  delay(1500);
  adjustMotors(180, 180, 180);
  delay(1500);
  adjustMotors(0, 0, 0);
  delay(1500);
  adjustMotors(115, 115, 115);
  delay(1500);
  adjustThrust(0);
  delay(1500);
  adjustThrust(131);
  delay(1500);
  adjustThrust(160);
  delay(1500);
  adjustThrust(250);
  delay(1500);
  adjustThrust(131);
  delay(1500);
  Serial.println("Completed Homing");
}

// Adjusts positions of all servo motors
void adjustMotors(int aimedTailTilt, int aimedRightTilt, int aimedLeftTilt) {
  adjustServoTilt(tailWingServo, aimedTailTilt);
  adjustServoTilt(leftWingServo, aimedLeftTilt);
  adjustServoTilt(rightWingServo, aimedRightTilt);
}

// Adjusts motor speed
void adjustThrust(int aimedThrustValue) {
  if (aimedThrustValue < 131) {
    aimedThrustValue = 0;
  }

  if (aimedThrustValue > thrustValue) {
    for (int i = thrustValue; i <= aimedThrustValue; i++) {
      analogWrite(motorEscPin, i);
      delay(35);
    }
  } else if (aimedThrustValue < thrustValue) {
    for (int i = thrustValue; i >= aimedThrustValue; i--) {
      analogWrite(motorEscPin, i);
      delay(35);
    }
  }
  thrustValue = aimedThrustValue;
}

// Adjusts tilt angle of a servo motor
void adjustServoTilt(Servo servo, int aimedTilt) {
  servo.writeMicroseconds(1990 / 180 * aimedTilt);
}

// Connects to the MQTT broker
void connectToBroker() {
  Serial.println("Connecting to cellular network");

  while (!Serial);
  boolean connected = false;
  // Attempt to connect to the cellular network
  while (!connected) {
    if ((nbAccess.begin(pin) == NB_READY) && (gprs.attachGPRS() == GPRS_READY)) {
      connected = true;
      Serial.println("Connected to cellular network");
    } else {
      Serial.print(".");
      delay(1000);
    }
  }

 // Attempt to connect to the MQTT broker
  Serial.println("Connecting to MQTT Broker");
  while (!client.connect(clientId)) {
    Serial.print(".");
    delay(1000);
  }
  client.subscribe(topic);

  Serial.println("Connected to MQTT Broker");
}

// Callback function called when a message is received
void messageReceived(String &topic, String &payload) {
  Serial.println("Incoming message: " + topic + " - " + payload);
}