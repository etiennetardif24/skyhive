  

<a name="readme-top"></a>

  

<!-- INFOGRAPHY -->

<div align="center">

  <h3 align="center">SkyHive</h3>

  <p align="center">

  

    An Open Source Aircraft flight path automation system

  

    <br />

    <a href=""><strong>Explore the documentation »</strong></a>

    <br />

  </p>

</div>

  
  

<!-- TABLE OF CONTENTS -->

<details>

  <summary>Content Table</summary>

  <ol>

    <li>

      <a href="#about-the-project">About the project</a>

      <ul>

        <li><a href="#built-using">Built using</a></li>

        <li><a href="#parts-used">Parts Used</a></li>

      </ul>

    </li>

    <li>

      <a href="#reference-schemes">Reference Schemes</a>

      <a href="#installation">Installation</a>

    </li>

    <li><a href="#roadmap">Roadmap</a></li>

    <li><a href="#license">License</a></li>

    <li><a href="#contact">Contact</a></li>

  </ol>

</details>

  

<!-- ABOUT THE PROJECT -->

## About the project

  

SkyHive is an Open Source project that aims to create a small and liteweight environnement to control multiple instances of the  R-OS1-LTE, an open source aircraft running on an Arduino ,  with an emphasis on automation. The project has three main goals:

  

### **Create a drone:** 

The first goal of this project is to:

Create a small planner aircraft named the R-OS1-LTE (‘R’ stands for Reconnaissance, ‘OS’ for Open Source, ‘1’ as it’s the first iteration of its kind, and ‘LTE’ for the interfacing system) that can be used for various purposes such as aerial photography, tracking, and delivery services in future instances.

### **Bidirectional communication with MQTT Broker:** 

The second goal of this project is to:

Create a MQTT Broker that will act as a bridge between the drone and the native interface to enable a bidirectional communication to remotely control the R-OS1-LTE.

### **Native app to change flight path:** 

The third goal of this project is to:

Create a native app that can connect to the MQTT Broker and therefore multiple instances of a R-OS1-LTE and change the flight path of each drone. This will allow users to control the drone’s movements and adjust settings from a desktop.

  
  

<!-- BUILT USING -->

## Built using:

* [![ASP.NET-Core][ASP.NET-Core.com]][ASP.NET-Core-url]

* [![Python][Python.com]][Python-url]

* [![Arduino][Arduino.cc]][Arduino-url]

<!-- PARTS USED -->

## Parts Used:

* 1 - Spektrum Avian 100 Amp Brushlee Smart ESC (https://www.spektrumrc.com/product/avian-100-amp-brushless-smart-esc-3s-6s/SPMXAE1100.html)

* 2 - Spektrum Firma 6500Kv Brushless Motor (https://www.spektrumrc.com/product/firma-6500kv-brushless-motor/SPMXSM2800.html)

* 3 - 35kg High Torque Coreless Servo Motor

* 1 - 4S 14,8 V 9000 mAh (https://www.amazon.ca/-/fr/dp/B08CDT3SR3?psc=1&ref=ppx_yo2ov_dt_b_product_details)

* 1 - Custom made spliter wire ( EC5 to EC5 and 2 wire **See reference schemes**)

* 2 - 3v-40v Current Converter (https://www.amazon.ca/dp/B09P15S7WZ/?coliid=I1VSRW8L5ZG3SS&colid=31VR5TP4A9CSJ&psc=1&ref_=cm_sw_r_cp_ud_lstpd_T3Z9MGM0GKV4BAC5KK66)

* 1 - Arduino MRK NB 1500 (https://store.arduino.cc/products/arduino-mkr-nb-1500)

* ∞ - Elbow Grease

  

<!-- REFERENCES -->

## Reference Schemes
Electrical Diagram
![[Diagramme Électrique.png]]
  

<!-- INSTALLATION -->

## Installation

  

#### Arduino [Arduino IDE](https://www.arduino.cc/en/software)

#### Python [Jetbrains Pycharm](https://www.jetbrains.com/rider/)

#### C# MQTT Broker [Jebrains Rider](https://www.jetbrains.com/rider/)

  
  

<!-- ROADMAP -->

## Roadmap

  

### Create a Drone

- [x] Create the Electronics

- [ ] Create the Chassis

  

### Create a MQTT Broker

- [x] Create the MQTT Broker

  

### Create a Native App

- [x] Create the interface

  
  

See the [open issues](https://gitlab.com/etiennetardif24/skyhive/-/issues) for a full list of proposed features (and known issues).

  
  

<!-- LICENSE -->

## License

  

Copyright (c) <year> <copyright holders>

  

Permission is hereby granted, free of charge, to any person obtaining a copy

of this software and associated documentation files (the "Software"), to deal

in the Software without restriction, including without limitation the rights

to use, copy, modify, merge, publish, distribute, sublicense, and/or sell

copies of the Software, and to permit persons to whom the Software is

furnished to do so, subject to the following conditions:

  

The above copyright notice and this permission notice shall be included in all

copies or substantial portions of the Software.

  

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR

IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,

FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE

AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER

LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,

OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

SOFTWARE.

  

<p align="right">(<a href="#readme-top">get back to top</a>)</p>

  
  

<!-- CONTACT -->

  

## Contact

  

Project Link: [https://gitlab.com/etiennetardif24/skyhive](https://gitlab.com/etiennetardif24/skyhive)

  
  

<p align="right">(<a href="#readme-top">get back to top</a>)</p>

  

<!-- MARKDOWN LINKS & IMAGES -->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

  
  

[Arduino.cc]: https://img.shields.io/badge/-Arduino-00979D?style=for-the-badge&logo=Arduino&logoColor=white

[Arduino-url]: https://www.arduino.cc/

  

[ASP.NET-Core.com]: https://img.shields.io/badge/ASP.NET_Core-512BD4?style=for-the-badge&logo=.net&logoColor=white

[ASP.NET-Core-url]: https://docs.microsoft.com/en-us/aspnet/core/

  

[Python.com]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54

[Python-url]: https://www.python.org/