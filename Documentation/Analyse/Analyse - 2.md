#SkyHive

```table-of-contents
```
---
## Introduction

Le projet SkyHive vise à créer un modèle de Drone Open Source (R-OS1-LTE, "R" signifiant reconnaissance pour le style d'aéronef, "OS1" pour la première itération Open Source, et "LTE" pour le protocole de communication internet). Ce drone utilise un Arduino MKR NB 1500 comme contrôleur, connecté à un Broker MQTT, et est contrôlable via une application native Python. L'application Python permet la gestion en temps réel de plusieurs instances de drones sous une même interface.

Ce document fournit une vue d'ensemble des éléments clés du projet mis à jour en date du 30 avril 2024.

---

## Standard de Programmation
**Seulement la page spécifier dans l'URL**
### Python

- [PEP 8 (Style Guide for Python Code)](https://peps.python.org/pep-0008/)

### Arduino (C/C++)

- [Arduino Coding Style Guide](https://docs.arduino.cc/learn/contributions/arduino-writing-style-guide/)

### ASP.NET Core Console App (C#)

- [Common C# Code Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)

---
## Environnement de Travail

### JetBrains Pycharm - Python
- [JetBrains Pycharm](https://www.jetbrains.com/pycharm/)

### Arduino IDE 2.0 - Arduino/C/C++
- [Arduino IDE 2.0](https://docs.arduino.cc/software/ide/#ide-v2)

### JetBrains Rider - ASP .NET Core Console App (C#)
- [JetBrains Rider](https://www.jetbrains.com/rider/)

---
## Application Python (Contrôle du R-OS1-LTE)

Application native Python avec une interface graphique utilisant le protocole MQTT pour établir un lien bidirectionnel avec une instance de R-OS1-LTE via le Broker MQTT.

### User Stories

En tant qu'utilisateur, je peux :

- Établir et terminer un lien entre l'application native et une instance de R-OS1-LTE.
- Visualiser en temps réel la position d'une instance de R-OS1-LTE (mise à jour toutes les 30 secondes au maximum, incluant la longitude, la latitude et l'altitude).
- Prédéfinir et mettre à jour le parcours d'une instance de R-OS1-LTE sur une carte dans l'interface.
- Contrôler en temps réel le parcours d'une instance de R-OS1-LTE.
- Faire décoller une instance de R-OS1-LTE.
- Faire atterrir une instance de R-OS1-LTE.

---
## R-OS1-LTE

Modèle de drone open source, comme décrit précédemment.

### Fonctionnalités

- Communication avec le Broker MQTT
- Interprétation des commandes reçues de le Broker MQTT.
- Transmission de l'état de ses modules (ailes, moteurs, longitude, latitude, altitude).
- Obtention de sa localisation (longitude, latitude, altitude).
- Ajustement du "tilt" des ailes droite et gauche, de la queue, et de la vitesse du moteur.

---
## Broker MQTT

Application Console ASP .Net Core utilisant le protocole MQTT pour établir un lien bidirectionnel entre une instance de R-OS1-LTE et l'application native Python.

### Fonctionnalités

- Réception des commandes de l'application des clients MQTT ( Application et Drones)

---
## Diagramme de Module

![[Diagramme de Module - v2.png]]

---
## Diagramme de classes
![[Diagramme de classes - v2.png]]

---

### Statut Actuel - 2024-04-30

L'avancement actuel du projet montre une progression significative sur plusieurs fronts. En ce qui concerne le drone lui-même, les fonctionnalités opérationnelles comprennent le contrôle des servomoteurs et de l'ESC du moteur principal, ainsi que la connexion et la communication avec un broker MQTT. Cependant, le défi réside dans l'interprétation des nouvelles commandes pour ajuster les moteurs en conséquence. Du côté de l'application Python, bien qu'elle soit encore en cours de développement, des fonctionnalités importantes sont déjà implémentées, notamment une carte affichant les drones et leur position, ainsi qu'une page de contrôle avec prise en charge d'un contrôleur physique. Cependant, les commandes ne sont pas encore envoyées au courtier MQTT, et la page de gestion des paramètres nécessite encore du travail. En ce qui concerne le broker MQTT, le développement est presque terminé, mais des difficultés subsistent concernant l'hébergement du service. J'envisage de passer d'un service tiers à mon propre hébergement pour plus de contrôle. Malgré des problèmes rencontrés, notamment liés au choix de protocole et de bibliothèque, le projet progresse vers la réalisation de son objectif de création d'un drone fonctionnel et d'une infrastructure logicielle robuste.