#SkyHive
# Table de contenu
- [[#Introduction|Introduction]]
- [[#Standard de Programmation|Standard de Programmation]]
	- [[#Standard de Programmation#Python|Python]]
	- [[#Standard de Programmation#Arduino (C/C++)|Arduino (C/C++)]]
	- [[#Standard de Programmation#ASP.NET Core Web API (C#)|ASP.NET Core Web API (C#)]]
- [[#Environnement de Travail|Environnement de Travail]]
	- [[#Environnement de Travail#JetBrains Pycharm - Python|JetBrains Pycharm - Python]]
	- [[#Environnement de Travail#Arduino IDE 2.0 - Arduino C/C++|Arduino IDE 2.0 - Arduino C/C++]]
	- [[#Environnement de Travail#JetBrains Rider - ASP .NET Core Web API (C#)|JetBrains Rider - ASP .NET Core Web API (C#)]]
- [[#Application Python (Contrôle du R-OS1-LTE)|Application Python (Contrôle du R-OS1-LTE)]]
	- [[#Application Python (Contrôle du R-OS1-LTE)#User Stories|User Stories]]
- [[#R-OS1-LTE|R-OS1-LTE]]
	- [[#R-OS1-LTE#Fonctionnalités|Fonctionnalités]]
- [[#API de Contrôle du Drone|API de Contrôle du Drone]]
	- [[#API de Contrôle du Drone#Fonctionnalités|Fonctionnalités]]
- [[#Diagramme de Module|Diagramme de Module]]
- [[#Diagramme de classes|Diagramme de classes]]


## Introduction

Le projet SkyHive vise à créer un modèle de Drone Open Source (R-OS1-LTE, "R" signifiant reconnaissance pour le style d'aéronef, "OS1" pour la première itération Open Source, et "LTE" pour le protocole de communication internet). Ce drone utilise un Arduino MKR NB 1500 comme contrôleur, connecté à une API web, et est contrôlable via une application native Python. L'application Python permet la gestion en temps réel de plusieurs instances de drones sous une même interface.

Ce document fournit une vue d'ensemble des éléments clés du projet.

---

## Standard de Programmation
**Seulement la page spécifier dans l'URL**
### Python

- [PEP 8 (Style Guide for Python Code)](https://peps.python.org/pep-0008/)

### Arduino (C/C++)

- [Arduino Coding Style Guide](https://docs.arduino.cc/learn/contributions/arduino-writing-style-guide/)

### ASP.NET Core Web API (C#)

- [Common C# Code Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)

---

## Environnement de Travail

### JetBrains Pycharm - Python

- [JetBrains Pycharm](https://www.jetbrains.com/pycharm/)

### Arduino IDE 2.0 - Arduino C/C++

- [Arduino IDE 2.0](https://docs.arduino.cc/software/ide/#ide-v2)

### JetBrains Rider - ASP .NET Core Web API (C#)

- [JetBrains Rider](https://www.jetbrains.com/rider/)

---

## Application Python (Contrôle du R-OS1-LTE)

Application native Python avec une interface graphique utilisant le protocole WebSocket pour établir un lien bidirectionnel avec une instance de R-OS1-LTE via l'api Web.

### User Stories

En tant qu'utilisateur, je peux :

- Établir et terminer un lien entre l'application native et une instance de R-OS1-LTE.
- Visualiser en temps réel la position d'une instance de R-OS1-LTE (mise à jour toutes les 30 secondes au maximum, incluant la longitude, la latitude et l'altitude).
- Prédéfinir et mettre à jour le parcours d'une instance de R-OS1-LTE sur une carte dans l'interface.
- Contrôler en temps réel le parcours d'une instance de R-OS1-LTE.
- Faire décoller une instance de R-OS1-LTE.
- Faire atterrir une instance de R-OS1-LTE.

---

## R-OS1-LTE

Modèle de drone open source, comme décrit précédemment.

### Fonctionnalités

- Communication avec l'API.
- Interprétation des commandes reçues de l'API.
- Transmission de l'état de ses modules (ailes, moteurs, longitude, latitude, altitude).
- Obtention de sa localisation (longitude, latitude, altitude).
- Ajustement du "tilt" des ailes droite et gauche, de la queue, et de la vitesse du moteur.

---

## API de Contrôle du Drone

API Web ASP .Net Core utilisant le protocole WebSocket pour établir un lien bidirectionnel entre une instance de R-OS1-LTE et l'application native Python.

### Fonctionnalités

- Réception des commandes de l'application Python.
- Interprétation des commandes de l'application Python.
- Transmission des commandes à l'Arduino MKR NB 1500 du R-OS1-LTE.
- Transmission de l'état d'une instance de R-OS1-LTE vers l'application Python.

---
## Diagramme de Module

![[Diagramme de Module - v1.png]]

---
## Diagramme de classes
![[Diagramme de classes - v1.png]]

---

### Statut Actuel - 2024-02-29

Actuellement, l'application Python a atteint un niveau avancé avec plusieurs caractéristiques notables. L'interface graphique multipages est réalisée en utilisant une version customisée de Tkinter, connue sous le nom de CTKinter. Cette personnalisation a été nécessaire pour répondre aux besoins spécifiques du projet SkyHive qui se voulais apporter une approche simple et moderne à une application de (travail).

J'ai rencontré des problèmes lors de l'utilisation de CTkinter. J'ai eu  recours à la fonction `After` plutôt qu'à une approche multithread classique a posé des défis, entraînant des bugs liés à la gestion des tâches simultanées.

Un autre défi majeur a été identifié lors de l'intégration du contrôleur Xbox. L'interaction avec le contrôleur provoquait un gel de l'application. J'ai contourner le problème en lançant la page de gestion du contrôleur sur un thread différent que celui utiliser par l'application principal j'ai pus permettre au deux instance de se lancer en même temps sans problème*.

Par ailleurs, des tests ont été effectués avec succès pour établir une connexion Internet via la carte SIM insérée dans l'Arduino MKR NB 1500, servant de contrôleur. J'ai pus connecter le drone à internet, Ping google et recevoir du HTML et même faire un envoie de SMS.

Malheureusement, le développement sur le drone a été temporairement interrompu en raison d'un board Arduino défectueux. J'ai contacté le support de chez arduino qui m'ont très gentiment fournis un nouveau Arduino MKR NB 1500 qui est en livraison.