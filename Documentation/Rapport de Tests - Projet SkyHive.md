### **Objectif :**
L'objectif de ce rapport est de donner un contexte pour tester les fonctionnalités du projet SkyHive.
### **1.  Scénarios de Test : Servomoteurs et Moteur**

1. **Scénario 1 : Test de la fonctionnalité des servomoteurs et des moteurs**
    - **Étapes :**
        1. Effectuer une routine de calibrage pour tester la fonctionnalité des servomoteurs et du moteur.
        2. Ajuster les positions des servomoteurs et du moteur.
    - **Résultat Attendu :**
        - Les fonctions des servomoteurs et du moteur doivent fonctionner comme prévu sans erreurs.

2. **Scénario 2 : Test de la fonctionnalité des servomoteurs et des moteurs en cas d'absence**
    - **Étapes :**
        1. Effectuer une routine de calibrage pour tester la fonctionnalité des servomoteurs et du moteur sans avoir les moteurs de connecter.
        2. Ajuster les positions des servomoteurs et du moteur.
    - **Résultat Attendu :**
        - Le code devrais signifier l'absence des servomoteurs et du moteur.

### **2. Scénario de Test : Broker MQTT**

1. **Scénario : Vérification de la connexion au broker MQTT**
    - **Étapes :**
        1. Initialiser le client MQTT.
        2. Connecter le client MQTT au broker.
        3. Vérifier la connexion réussie.
    - **Résultat Attendu :**
        - Le client MQTT se connecte avec succès au broker sans erreurs.

2. **Scénario : Vérification de l'envoie de message au broker MQTT**
    - **Étapes :**
        1. Initialiser le client MQTT.
        2. Connecter le client MQTT au broker.
        3. Envoyer un message au broker avec un topic de test.
    - **Résultat Attendu :**
        - Le message est reçu par le broker MQTT et est accessible par les autres clients.

3. **Scénario : Vérification de l'initialisation du broker MQTT
    - **Étapes :**
        1. Initialiser le broker MQTT.
        2. Connecter un client MQTT au broker.
        3. Vérifier que la connexion est possible.
    - **Résultat Attendu :**
        - Le client MQTT se connecte avec succès au broker sans erreurs.

4. **Scénario : Vérification de la réception des messages sur le broker MQTT
    - **Étapes :**
        1. Initialiser le broker MQTT.
        2. Connecter un client MQTT au broker.
        3. Envoyer un message au broker avec un topic de test.
    - **Résultat Attendu :**
        - Le broker MQTT reçois le message et le rends disponible au autres clients.

### 3. Scénario de Test : Application Python**

1. **Scénario : Connexion à un drone**
    - **Étapes :**
        1. Accéder à la page de contrôle des drones.
        2. Sélectionner l'option de connexion à un nouveau drone.
        3. Choisir le drone auquel nous souhaitons nous connecter.
        4. Valider la connexion.
    - **Résultat Attendu :**
        - Le drone se connecte avec succès à l'application, il est ajouté à la liste de drone et ses données sont disponibles pour le contrôle et la surveillance.

2. **Scénario : Déconnexion d'un drone**
    - **Étapes :**
        1. Accéder à la page de contrôle des drones.
        2. Sélectionner le drone à déconnecter dans la liste de drone connecté.
        3. Sélectionner l'option de déconnexion.
    - **Résultat Attendu :**
        - Le drone est déconnecté avec succès de l'application, il retombe disponible pour une autre instance de l'application et ses données ne sont plus disponibles pour le contrôle.

3. **Scénario : Contrôle d'un drone**
    - **Étapes :**
        1. Accéder à la page de contrôle des drones.
        2. Sélectionner le drone à contrôler.
        3. Utiliser les commandes de contrôle à l'aide d'une manette de XBOX pour piloter le drone.
    - **Résultat Attendu :**
        - Le drone répond correctement aux commandes de contrôle et effectue les actions demandées.

4. **Scénario : Obtention des données d'un drone**
    - **Étapes :**
        1. Accéder à la page d'informations sur les drones.
        2. Consulter les données telles que la position GPS, la vitesse, l'altitude, etc.
    - **Résultat Attendu :**
        - Les données du drone sélectionné sont correctement affichées et mises à jour en temps réel.

5. **Scénario : Création d'un "Flight path" de drone**
    - **Étapes :**
        1. Accéder à la page de contrôle des drones.
        2. Sélectionner le drone pour lequel créer le "flight path".
        3. Définir les points de passage à effectuer à chaque point.
        4. Valider la création du "flight path".
    - **Résultat Attendu :**
        - Le "flight path" est créé avec succès et le drone est capable de le suivre en vol, en effectuant les actions prévues à chaque point de passage.

6. **Scénario : Affichage d'un flight path**
    - **Étapes :**
        1. Accéder à la page d'accueil de l'application.
        5. Valider que le "flight path" du drone est bel et bien présent sur la carte.
    - **Résultat Attendu :**
        - Le "flight path" est visible sur la carte dans la page d'accueil.