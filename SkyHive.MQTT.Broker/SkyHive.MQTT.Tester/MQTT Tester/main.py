import random
import time
import logging

from paho.mqtt import client as mqtt_client

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# MQTT broker details
broker = 'broker.hivemq.com'
port = 1883
topic = "test"


def connect_mqtt():
    """
    Connect to the MQTT broker and set up callback functions.
    """
    def on_connect(client, userdata, flags, reason_code, properties):
        """
        Callback function called when the client connects to the broker.
        """
        if flags.session_present:
            logger.info('Session already present.')
        if reason_code == 0:
            logger.info('Connection successful.')
        else:
            logger.error(f'Connection failed with code {reason_code}')

    def on_disconnect(client, userdata, flags, reason_code, properties):
        """
        Callback function called when the client disconnects from the broker.
        """
        if reason_code == 0:
            logger.info('Disconnected gracefully.')
        else:
            logger.error(f'Disconnected unexpectedly with code {reason_code}')

    def on_subscribe(client, userdata, mid, reason_codes, properties):
        """
        Callback function called when the client subscribes to a topic.
        """
        for sub_result in reason_codes:
            if sub_result == 0:
                logger.info('Subscription successful.')
            else:
                logger.error(f'Subscription failed with code {sub_result}')

    def on_message(client, userdata, message):
        """
        Callback function called when a message is received from the broker.
        """
        logger.info(f"Received message: {message.payload.decode()} on topic {message.topic}")

    client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION2, f'python-mqtt-{random.randint(0, 1000)}')
    client.on_connect = on_connect
    client.on_message = on_message  # Set the on_message callback function
    client.connect(broker, port)
    client.subscribe(topic)  # Subscribe to the topic
    return client


def publish(client):
    """
    Publish messages to the MQTT broker at regular intervals.
    """
    msg_count = 0
    while True:
        time.sleep(1)
        msg = f"messages: {msg_count}"
        result, _ = client.publish(topic, msg)
        if result == mqtt_client.MQTT_ERR_SUCCESS:
            logger.info(f"Sent `{msg}` to topic `{topic}`")
        else:
            logger.error(f"Failed to send message to topic {topic}")
        msg_count += 1


def run():
    """
    Main function to connect to the MQTT broker and start publishing messages.
    """
    client = connect_mqtt()
    client.loop_forever()


if __name__ == '__main__':
    run()
