﻿using System;
using System.Threading.Tasks;
using MQTTnet;
using MQTTnet.Server;

namespace SkyHive.MQTT.Broker
{
    internal class Program
    {
        /// <summary>
        /// Simple MQTT Broker used to host the messages sent by the clients of the SkyHive application.
        /// This code should be hosted on a machine accessible via internet to give a communication endpoint to the SkyHive Application and the R-OS1-LTE Drones.
        /// </summary>
        /// <param name="args"></param>
        static async Task Main(string[] args)
        {
            var mqttFactory = new MqttFactory();

            // The port for the default endpoint is 1883.
            // The default endpoint is not encrypted

            //TODO Change the default endpoint port for final deployment
            // new MqttServerOptionsBuilder()
            //     .WithDefaultEndpoint()
            //     .WithDefaultEndpointPort(1234)
            //     .Build();
            
            var mqttServerOptions = new MqttServerOptionsBuilder().WithDefaultEndpoint().Build();
            
            using (var mqttServer = mqttFactory.CreateMqttServer(mqttServerOptions))
            {
                await mqttServer.StartAsync();

                Console.WriteLine("Press Enter to close the broker");
                Console.ReadLine();
                
                await mqttServer.StopAsync();
            }
        }
    }
}