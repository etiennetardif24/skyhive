import customtkinter as ctk
from PIL import Image
from pages.main_page import MainPage
from pages.drone_page import DronesPage
from models.drone import Drone
from pages.settings_page import SettingsPage
import random


def create_nav_button(master, text, image, command):
    """
    Helper function to create navigation buttons.

    Parameters:
    - master: The master widget.
    - text: The text on the button.
    - image: The image for the button.
    - command: The function to be executed on button click.

    Returns:
    - ctk.CTkButton: The created button.
    """
    return ctk.CTkButton(
        master=master,
        text=text,
        image=image,
        font=("Helvetica", 14, "bold"),
        command=command,
    )


class MyApp:
    """
    Main application class for SkyHive Management.
    """

    def __init__(self):
        """
        Initializes the application.
        """

        self.main_page = None
        self.selected_page = None  # Variable to store the selected page
        self.drones_page = None

        self.app = ctk.CTk()
        self.drones = []  # Initialize drones as an empty list

        # Navigation frame
        self.nav_frame = ctk.CTkFrame(master=self.app, width=200, fg_color="gray5", corner_radius=0)
        self.nav_label = ctk.CTkLabel(master=self.nav_frame, text="SkyHive", font=("Helvetica", 16, "bold"))
        self.version_label = ctk.CTkLabel(master=self.nav_frame, text="V 0.0.1", font=("Helvetica", 11, "bold"))

        # Buttons for navigation frame
        self.main_button = None
        self.drones_button = None
        self.settings_button = None
        self.update_button = None

        # Content frame
        self.content_frame = ctk.CTkFrame(master=self.app)

        # Icons for buttons
        main_icon = ctk.CTkImage(dark_image=Image.open(r"./assets/home_FILL0_wght400_GRAD0_opsz24.png"))
        drone_icon = ctk.CTkImage(dark_image=Image.open(r"./assets/travel_FILL0_wght400_GRAD0_opsz24.png"))
        setting_icon = ctk.CTkImage(dark_image=Image.open(r"./assets/settings_FILL0_wght400_GRAD0_opsz24.png"))

        # Initialize and configure buttons with icons
        self.create_navigation_frame(main_icon, drone_icon, setting_icon)

        # Initialize and configure the application
        self.initialize_app()

    def initialize_app(self):
        """
        Initializes the application.
        """
        self.app.title("SkyHive Management V 0.0.1")
        self.app.geometry("800x600")
        self.app.minsize(800, 600)

        # Create a list of drones with random longitude, latitude, and altitude
        self.drones = [Drone(f'{i}ROS1LTE') for i in range(12)]

        # Set random values for longitude, latitude, and altitude for each drone
        for drone in self.drones:
            drone.longitude = round(random.uniform(-180.00000, 180.00000), 6)
            drone.latitude = round(random.uniform(-90.00000, 90.00000), 6)
            drone.altitude = round(random.uniform(0.000, 1000.000), 3)

        # Pack the navigation frame
        self.nav_frame.pack(side="left", fill="y")

        # Pack labels in navigation frame
        self.nav_label.pack(side="top", padx=10, pady=10)
        self.version_label.pack(side="top")

        # Pack the buttons vertically
        self.main_button.pack(pady=5, padx=10)
        self.drones_button.pack(pady=5, padx=10)
        self.settings_button.pack(side="bottom", pady=5, padx=10)
        self.update_button.pack(side="bottom", pady=5, padx=10)

        # Pack the content frame
        self.content_frame.pack(side="right", fill="both", expand=True)

        # Show the Main page by default
        self.show_main_page()

        # Call update_info_periodically every 5 seconds
        self.app.after(1000, self.update_info_periodically)

    def create_navigation_frame(self, main_icon, drone_icon, setting_icon):
        """
        Creates the navigation frame.

        Parameters:
        - main_icon: The icon for the Main button.
        - drone_icon: The icon for the Drones button.
        - setting_icon: The icon for the Settings button.
        """
        self.main_button = create_nav_button(self.nav_frame, "Main", main_icon, self.show_main_page)
        self.drones_button = create_nav_button(self.nav_frame, "Drones", drone_icon, self.show_drone_page)
        self.settings_button = create_nav_button(self.nav_frame, "Settings", setting_icon, self.show_settings_page)
        self.update_button = create_nav_button(self.nav_frame, "Update", setting_icon, self.simulate_drone_update)

    def show_main_page(self):
        """
        Shows the Main page.
        """
        self.clear_content_frame()
        self.selected_page = "Main"
        self.main_page = MainPage(self.content_frame, self.drones)

    def show_drone_page(self):
        """
        Shows the Drones page.
        """
        self.clear_content_frame()
        self.selected_page = "Drones"
        self.drones_page = DronesPage(self.content_frame, self.drones)
        self.drones_page.update_page()

    def show_settings_page(self):
        """
        Shows the Settings page.
        """
        self.clear_content_frame()
        SettingsPage(self.content_frame)

    def clear_content_frame(self):
        """
        Clears existing content in the content frame.
        """
        for widget in self.content_frame.winfo_children():
            widget.destroy()

    def update_info_periodically(self):
        """
        Update information periodically.
        """
        if self.selected_page == "Drones":
            self.drones_page.update_page()
        if self.selected_page == "Main":
            self.main_page.update_page()
        self.app.after(1000, self.update_info_periodically)

    def simulate_drone_update(self):
        """
        Simulates the update of drone information for testing purposes.
        """
        for drone in self.drones:
            # Simulate updating drone information
            drone.longitude = round(random.uniform(-180.00000, 180.00000), 6)
            drone.latitude = round(random.uniform(-90.00000, 90.00000), 6)
            drone.altitude = round(random.uniform(0.000, 1000.000), 3)


if __name__ == "__main__":
    app_instance = MyApp()
    app_instance.app.mainloop()
