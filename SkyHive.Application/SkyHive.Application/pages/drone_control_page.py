import customtkinter as ctk
from utilities.xbox_controller import XboxController
import threading

class DroneControlPage:
    """
    Represents the Drone Control Page of the application.

    Attributes:
    - drone: The Drone instance associated with this control page.
    - app: The main application window.
    - controller: The XboxController instance for handling input.
    - drone_frame: The frame containing drone information labels.
    - information_frame: The main frame for displaying information.
    - controller_frame: The frame containing XboxController-related widgets.
    - map_path_frame: The frame for displaying the map and path.
    - label_buttons: Label for displaying controller button states.
    - label_dpad: Label for displaying D-pad states.
    - left_joystick_frame: Frame for the left joystick diagram.
    - right_joystick_frame: Frame for the right joystick diagram.
    - left_circle_canvas: Canvas for the left joystick diagram.
    - right_circle_canvas: Canvas for the right joystick diagram.
    - circle_outline: Outline of the joystick circle.
    - center_dot: Center dot of the joystick circle.
    - stop_thread: Flag to stop the controller state update thread.
    """

    def __init__(self, drone):
        """
        Initializes the DroneControlPage instance.

        Parameters:
        - drone: The Drone instance associated with this control page.
        """
        self.drone = drone
        self.app = ctk.CTk()
        self.app.title(f"Drone Control - {drone.id}")
        self.app.geometry("800x800")

        self.controller = XboxController()

        self.drone_frame = ctk.CTkFrame(self.app, width=250)
        self.drone_frame.pack(side="left", fill='both', pady=5, padx=5, expand=True)

        # Labels for drone information
        self.title_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 14, "bold"))
        self.content_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))
        self.left_tilt_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))
        self.right_tilt_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))
        self.tail_tilt_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))
        self.thrust_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))
        self.battery_label = ctk.CTkLabel(self.drone_frame, text="", font=("Helvetica", 12))

        self.information_frame = ctk.CTkFrame(self.app)
        self.information_frame.pack(side="right", fill='both', pady=5, padx=5, expand=False)

        self.controller_frame = ctk.CTkFrame(self.information_frame)
        self.controller_frame.pack(side="top", fill='both', pady=5, padx=5)

        self.map_path_frame = ctk.CTkFrame(self.information_frame)
        self.map_path_frame.pack(side="bottom", fill='both', pady=5, padx=5, expand=True)

        self.label_buttons = ctk.CTkLabel(self.controller_frame, text="")
        self.label_buttons.pack()

        self.label_dpad = ctk.CTkLabel(self.controller_frame, text="")
        self.label_dpad.pack()

        # Create a new frame for the circle diagram
        self.left_joystick_frame = ctk.CTkFrame(self.controller_frame)
        self.left_joystick_frame.pack(side="left")

        self.right_joystick_frame = ctk.CTkFrame(self.controller_frame)
        self.right_joystick_frame.pack(side="right")

        # Create a canvas for the circle diagram
        self.left_circle_canvas = ctk.CTkCanvas(self.left_joystick_frame, width=200, height=200, bg='gray9')
        self.left_circle_canvas.pack()

        self.right_circle_canvas = ctk.CTkCanvas(self.right_joystick_frame, width=200, height=200, bg='gray9')
        self.right_circle_canvas.pack()

        # Update the circle diagram for the joysticks
        radius = 50
        center_x, center_y = 100, 100

        # Create the joystick circle as an object
        self.circle_outline = self.left_circle_canvas.create_oval(center_x - radius, center_y - radius,
                                                                  center_x + radius, center_y + radius, outline="white")
        self.center_dot = self.left_circle_canvas.create_oval(center_x - 5, center_y - 5, center_x + 5, center_y + 5,
                                                              fill="steel blue")

        self.circle_outline = self.right_circle_canvas.create_oval(center_x - radius, center_y - radius,
                                                                   center_x + radius, center_y + radius,
                                                                   outline="white")
        self.center_dot = self.right_circle_canvas.create_oval(center_x - 5, center_y - 5, center_x + 5, center_y + 5,
                                                               fill="steel blue")

        # Create and pack widgets
        self.create_widgets()

        self.stop_thread = False
        my_thread = threading.Thread(target=self.update_controller_state)
        self.app.protocol("WM_DELETE_WINDOW", self.on_window_close)
        my_thread.start()
        self.app.mainloop()

    def update_controller_state(self):
        """
        Continuously updates the controller state and drone information.
        """
        while not self.stop_thread:
            controller_button_state = f'A: {self.controller.A}, B: {self.controller.B}, X: {self.controller.X}, Y: {self.controller.Y}'
            controller_dpad_state = f'Up: {self.controller.UpDPad}, Down: {self.controller.DownDPad}, Left: {self.controller.LeftDPad}, Right: {self.controller.RightDPad}'

            # Update the circle diagram for the joysticks
            radius = 50

            # Convert joystick values to coordinates
            left_joystick_x = self.controller.LeftJoystickX * radius
            left_joystick_y = -self.controller.LeftJoystickY * radius
            right_joystick_x = self.controller.RightJoystickX * radius
            right_joystick_y = -self.controller.RightJoystickY * radius

            # Update the joystick circle position
            self.right_circle_canvas.coords(self.center_dot, 100 + right_joystick_x - 5, 100 + right_joystick_y - 5,
                                            100 + right_joystick_x + 5, 100 + right_joystick_y + 5)
            self.left_circle_canvas.coords(self.center_dot, 100 + left_joystick_x - 5, 100 + left_joystick_y - 5,
                                           100 + left_joystick_x + 5, 100 + left_joystick_y + 5)

            # Clear the canvas content
            self.right_circle_canvas.update()
            self.left_circle_canvas.update()

            self.label_buttons.configure(text=controller_button_state)
            self.label_dpad.configure(text=controller_dpad_state)

            # Convert joystick values to tilts
            left_tilt = self.controller.LeftJoystickX  # Adjust this mapping as needed
            right_tilt = self.controller.RightJoystickX  # Adjust this mapping as needed
            tail_tilt = self.controller.LeftJoystickY  # Adjust this mapping as needed

            # Adjust thrust based on the right trigger
            aimed_thrust_value = self.controller.RightTrigger  # Adjust this mapping as needed

            # Update labels with drone information
            self.title_label.configure(text=f"Drone {self.drone.id}")
            self.content_label.configure(
                text=f"Longitude | Latitude\n ({self.drone.latitude}, "
                     f"{self.drone.longitude})\n \nAltitude: {self.drone.altitude} meters")
            self.left_tilt_label.configure(text=f"Left Tilt: {self.drone.left_tilt}")
            self.right_tilt_label.configure(text=f"Right Tilt: {self.drone.right_tilt}")
            self.tail_tilt_label.configure(text=f"Tail Flap Tilt: {self.drone.tail_tilt}")
            self.thrust_label.configure(text=f"Thruster %: {self.drone.thrust_value}")
            self.battery_label.configure(text=f"Battery %: {self.drone.battery_percentage}")

            # Call the adjust methods with the mapped values
            self.drone.adjust_left_tilt(left_tilt)
            self.drone.adjust_right_tilt(right_tilt)
            self.drone.adjust_tail_tilt(tail_tilt)
            self.drone.adjust_thrust(aimed_thrust_value)

            self.app.update()

    def create_widgets(self):
        """
        Creates and packs labels for displaying drone information.
        """

        # Title label for the drone ID
        self.title_label.pack(pady=5, padx=5)

        # Content label for drone information
        self.content_label.pack(pady=5, padx=5)

        # Left Tilt label for drone information
        self.left_tilt_label.pack(pady=5, padx=5)

        # Right Tilt label for drone information
        self.right_tilt_label.pack(pady=5, padx=5)

        # Tail Tilt label for drone information
        self.tail_tilt_label.pack(pady=5, padx=5)

        # Thrust label for drone information
        self.thrust_label.pack(pady=5, padx=5)

        # Battery label for drone information
        self.battery_label.pack(pady=5, padx=5)

    def on_window_close(self):
        """
        Handles the window close event.
        """
        self.stop_thread = True
        self.app.destroy()
