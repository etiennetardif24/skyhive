import customtkinter as ctk
from tkintermapview import TkinterMapView
from components.drone_card import DroneCard


class MainPage:
    """
    Represents the main page of the SkyHive Management application.

    Attributes:
    - parent_frame: The parent frame where the main page is displayed.
    - drones: A list of Drone instances.
    - drones_instances_panel: The panel displaying information about running drones.
    - map_widget: TkinterMapView widget for displaying the map.
    - drone_cards: List of DroneCard instances.
    """

    def __init__(self, parent_frame, drones):
        """
        Initializes the MainPage instance.

        Parameters:
        - parent_frame: The parent frame where the main page is displayed.
        - drones: A list of Drone instances.
        """
        self.map_widget = None
        self.parent_frame = parent_frame
        self.drone_cards = []
        self.drones = drones  # Use the provided drone list
        self.drones_instances_panel = ctk.CTkFrame(
            master=self.parent_frame, fg_color="gray9", corner_radius=0
        )
        self.display_content()

    def display_content(self):
        """
        Displays the content of the main page.
        """
        # Create and configure the drones instances panel
        self.drones_instances_panel.pack(side="top", fill="x")
        self.drones_instances_panel.pack_propagate(False)

        # Create and configure the label for running drones
        settings_label = ctk.CTkLabel(
            master=self.drones_instances_panel,
            text="Running Drones",
            font=("Helvetica", 16, "bold"),
        )
        settings_label.pack(side="top", anchor="nw", padx=10, pady=10)

        # Create scrollable card panel for drone cards
        scrollable_card_panel = ctk.CTkScrollableFrame(
            self.drones_instances_panel, fg_color="gray9", orientation="horizontal"
        )
        scrollable_card_panel.pack(side="top", fill="x")

        # Create the TkinterMapView widget for the map (lower half)
        self.map_widget = TkinterMapView(self.parent_frame, corner_radius=8)
        self.map_widget.pack(fill="both", expand=True, pady=20, padx=20)
        self.map_widget.set_tile_server(
            "https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga", max_zoom=22
        )

        # Set the initial position and zoom level (e.g., Paris, France)
        self.map_widget.set_position(48.860381, 2.338594)  # Paris, France
        self.map_widget.set_zoom(15)

        for drone in self.drones:  # Iterate through the provided drone list
            # Extract drone information
            ping_latitude, ping_longitude = drone.latitude, drone.longitude
            ping_text = f"Drone {drone.id}"  # Customize the text for your marker

            # Set marker on the map
            self.map_widget.set_marker(ping_latitude, ping_longitude, text=ping_text)
            # Create a DroneCard for each drone in the list
            temp_card = DroneCard(master=scrollable_card_panel)
            temp_card.set_drone_info(drone)
            temp_card.pack(side="left", padx=10, pady=10, expand=False)
            self.drone_cards.append(temp_card)

    def update_page(self):
        """
        Update the MainPage and associated drone cards with the latest drone information.
        """
        self.map_widget.delete_all_marker()
        for drone_card in self.drone_cards:
            # Extract drone information
            ping_latitude, ping_longitude = drone_card.drone.latitude, drone_card.drone.longitude
            ping_text = f"Drone {drone_card.drone.id}"  # Customize the text for your marker

            self.map_widget.set_marker(ping_latitude, ping_longitude, text=ping_text)
            drone_card.set_drone_info(drone_card.drone)
