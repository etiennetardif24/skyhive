import customtkinter as ctk


class SettingsPage:
    def __init__(self, parent_frame):
        """
        Initializes the SettingsPage instance.

        Parameters:
        - parent_frame: The parent frame in which the SettingsPage is displayed.
        """
        self.parent_frame = parent_frame
        self.display_content()

    def display_content(self):
        """
        Displays the content of the SettingsPage.
        """
        settings_label = ctk.CTkLabel(master=self.parent_frame, text="Settings Page", font=("Helvetica", 16, "bold"))
        settings_label.pack(side="top", anchor="nw", padx=10, pady=10)
