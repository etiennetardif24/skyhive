import customtkinter as ctk
from components.drone_card_advanced import DroneCard


class DronesPage:
    """
    Represents the Drones page of the application.

    Attributes:
    - parent_frame: The parent frame where the content will be displayed.
    - drones: List of drone instances to be displayed.
    - drone_cards: List of DroneCard instances.
    """

    def __init__(self, parent_frame, drones):
        """
        Initializes the DronesPage instance.

        Parameters:
        - parent_frame: The parent frame where the content will be displayed.
        - drones: List of drone instances to be displayed.
        """
        self.drone_cards = []
        self.parent_frame = parent_frame
        self.drones = drones
        self.display_content()

    def display_content(self):
        """
        Displays the content on the Drones page.
        """
        main_label = ctk.CTkLabel(
            master=self.parent_frame, text="Drone Page", font=("Helvetica", 16, "bold")
        )
        main_label.pack(side="top", anchor="nw", padx=10, pady=10)

        # Create a frame for the canvas and buttons
        top_frame = ctk.CTkFrame(
            master=self.parent_frame, fg_color="transparent", corner_radius=0
        )
        top_frame.pack(side="top")

        # Create buttons for Connection and Disconnection
        connect_button = ctk.CTkButton(
            master=top_frame, text="Connect", font=("Helvetica", 16, "bold"), command=self.connect_drones
        )
        disconnect_button = ctk.CTkButton(
            master=top_frame, text="Disconnect", font=("Helvetica", 16, "bold"), command=self.disconnect_drones
        )

        # Pack the buttons side by side
        connect_button.pack(side="left", pady=5, padx=10)
        disconnect_button.pack(side="left", pady=5, padx=10)

        # Create a scrollable frame to hold the DroneCard instances
        scroll_frame = ctk.CTkScrollableFrame(
            master=self.parent_frame, fg_color="gray9", corner_radius=0
        )
        scroll_frame.pack(side="top", fill="both", expand=True)

        # Determine the number of columns based on the window's width
        max_card_width = 175
        num_columns = max(1, self.parent_frame.winfo_width() // max_card_width)

        # Create DroneCard instances and add them to the frame using the global drone list
        for index, drone in enumerate(self.drones):
            temp_card = DroneCard(master=scroll_frame, width=max_card_width, height=150)
            temp_card.set_drone_info(drone)

            # Use grid to create the desired layout
            temp_card.grid(
                row=index // num_columns, column=index % num_columns, padx=10, pady=10, sticky="w"
            )
            self.drone_cards.append(temp_card)

    def update_page(self):
        """
        Update the DronesPage and associated drone cards with the latest drone information.
        """
        for drone_card in self.drone_cards:
            drone_card.set_drone_info(drone_card.drone)

    def connect_drones(self):
        """
        Initiates the connection of drones.
        """
        # Simple prompt for connection
        app = ctk.CTk()
        app.title("Connect Drones")
        app.geometry("300x150")

        # TODO Add Connection Logic

        app.mainloop()

    def disconnect_drones(self):
        """
        Initiates the disconnection of drones.
        """
        # Simple prompt for disconnection
        app = ctk.CTk()
        app.title("Disconnect Drones")
        app.geometry("300x150")

        # TODO Add Disconnection Logic

        app.mainloop()

    def perform_connect(self):
        """
        Performs the connection of selected drones.

        Parameters:
        - drone_var_list: List of variables representing drone connections.
        - disconnect_prompt: The connect prompt window.
        """
        # TODO Add Connection Logic

    def perform_disconnect(self):
        """
        Performs the disconnection of selected drones.

        Parameters:
        - drone_var_list: List of variables representing drone connections.
        - disconnect_prompt: The disconnect prompt window.
        """
        # TODO Add Disconnection Logic
