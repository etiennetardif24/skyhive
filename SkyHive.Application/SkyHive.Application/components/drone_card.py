import threading

import customtkinter as ctk
from pages.drone_control_page import DroneControlPage


class DroneCard(ctk.CTkFrame):
    """
    Represents a custom Tkinter frame for displaying detailed information about a drone.

    Attributes:
    - title_label: Label for displaying the drone ID.
    - content_label: Label for displaying general drone information.
    - control_button: Button for drone control.
    """

    def __init__(self, master=None, **kwargs):
        """
        Initializes a DroneCard instance.

        Parameters:
        - master: The master widget.
        - **kwargs: Additional keyword arguments.
        """
        super().__init__(master, **kwargs)

        self.drone = None

        # Widgets for displaying drone information
        self.title_label = ctk.CTkLabel(self, text="", font=("Helvetica", 14, "bold"))
        self.content_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.control_button = ctk.CTkButton(self, text="Control", font=("Helvetica", 12, "bold"),
                                            command=self.open_drone_control_page)

        # Create and pack widgets
        self.create_widgets()

    def create_widgets(self):
        """
        Creates and packs labels and buttons for displaying drone information.
        """
        # Title label for the drone ID
        self.title_label.pack(side="top", pady=5, padx=5)

        # Content label for drone information
        self.content_label.pack(side="top", pady=5, padx=5)

        # Control button for drone
        self.control_button.pack(pady=5, padx=5)

    def set_drone_info(self, drone):
        """
        Sets the drone information on the title and content labels.

        Parameters:
        - drone: The Drone instance containing information.
        """
        self.drone = drone

        # Update labels with drone information
        self.title_label.configure(text=f"Drone {self.drone.id}")
        self.content_label.configure(
            text=f"Location: ({self.drone.latitude}, {self.drone.longitude})\nAltitude: {self.drone.altitude} meters"
        )

    def open_drone_control_page(self):
        thread = threading.Thread(target=self.create_drone_control_page)
        thread.start()

    def create_drone_control_page(self):
        """
        Execute the DroneControlPage on a new thread.
        """
        drone_control_page = DroneControlPage(self.drone)
