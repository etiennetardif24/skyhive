import threading

import customtkinter as ctk
from PIL import Image
from pages.drone_control_page import DroneControlPage


class DroneCard(ctk.CTkFrame):
    """
    Represents a custom Tkinter frame for displaying detailed information about a drone.

    Attributes:
    - title_label: Label for displaying the drone ID.
    - content_label: Label for displaying general drone information.
    - left_tilt_label: Label for displaying the left wing tilt.
    - right_tilt_label: Label for displaying the right wing tilt.
    - tail_tilt_label: Label for displaying the tail flap tilt.
    - thrust_label: Label for displaying the thruster percentage.
    - battery_label: Label for displaying the battery percentage.
    - control_button: Button for drone control.
    """

    def __init__(self, master=None, **kwargs):
        """
        Initializes a DroneCard instance.

        Parameters:
        - master: The master widget.
        - **kwargs: Additional keyword arguments.
        """
        super().__init__(master, **kwargs)

        # Drone icon
        self.drone = None
        drone_icon = ctk.CTkImage(dark_image=Image.open("./assets/travel_FILL0_wght400_GRAD0_opsz24.png"))

        # Labels for drone information
        self.title_label = ctk.CTkLabel(self, text="", font=("Helvetica", 14, "bold"))
        self.icon = ctk.CTkLabel(self, font=("Helvetica", 14, "bold"), text="", anchor="nw", image=drone_icon)
        self.content_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.left_tilt_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.right_tilt_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.tail_tilt_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.thrust_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.battery_label = ctk.CTkLabel(self, text="", font=("Helvetica", 12))
        self.control_button = ctk.CTkButton(self, text="Control", font=("Helvetica", 12, "bold"),
                                            command=self.open_drone_control_page)

        # Create and pack widgets
        self.create_widgets()

    def create_widgets(self):
        """
        Creates and packs labels for displaying drone information.
        """

        # Drone icon
        self.icon.pack(pady=5, padx=5)

        # Title label for the drone ID
        self.title_label.pack(pady=5, padx=5)

        # Content label for drone information
        self.content_label.pack(pady=5, padx=5)

        # Left Tilt label for drone information
        self.left_tilt_label.pack(pady=5, padx=5)

        # Right Tilt label for drone information
        self.right_tilt_label.pack(pady=5, padx=5)

        # Tail Tilt label for drone information
        self.tail_tilt_label.pack(pady=5, padx=5)

        # Thrust label for drone information
        self.thrust_label.pack(pady=5, padx=5)

        # Battery label for drone information
        self.battery_label.pack(pady=5, padx=5)

        # Control button for drone
        self.control_button.pack(pady=5, padx=5)

    def set_drone_info(self, drone):
        """
        Sets the drone information on the labels.

        Parameters:
        - drone: The Drone instance containing information.
        """
        self.drone = drone

        # Update labels with drone information
        self.title_label.configure(text=f"Drone {self.drone.id}")
        self.content_label.configure(
            text=f"Longitude | Latitude\n ({self.drone.latitude}, "
                 f"{self.drone.longitude})\n \nAltitude: {self.drone.altitude} meters")
        self.left_tilt_label.configure(text=f"Left Tilt: {self.drone.left_tilt}")
        self.right_tilt_label.configure(text=f"Right Tilt: {self.drone.right_tilt}")
        self.tail_tilt_label.configure(text=f"Tail Flap Tilt: {self.drone.tail_tilt}")
        self.thrust_label.configure(text=f"Thruster %: {self.drone.thrust_value}")
        self.battery_label.configure(text=f"Battery %: {self.drone.battery_percentage}")

    def open_drone_control_page(self):
        thread = threading.Thread(target=self.create_drone_control_page)
        thread.start()

    def create_drone_control_page(self):
        """
        Execute the DroneControlPage on a new thread.
        """
        drone_control_page = DroneControlPage(self.drone)
